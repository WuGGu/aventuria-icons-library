# License

This repository contains icons from the card game **Aventuria**, which are used with the kind permission of **Ulisses Medien & Spiel Distribution GmbH**. All rights to these icons are owned by Ulisses Medien & Spiel Distribution GmbH.

## Icons Usage Permission

Ulisses Medien & Spiel Distribution GmbH
Industriestraße 11
65529 Waldems / Steinfischbach

The icons from Aventuria are included in this repository with the express permission of Ulisses Medien & Spiel Distribution GmbH for the sole purpose of creating and maintaining a database of all cards from the Aventuria card game. 

## Terms and Conditions

1. **Attribution**: You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.

2. **Non-Commercial**: You may not use the icons for commercial purposes.

3. **No Derivatives**: If you remix, transform, or build upon the icons, you may not distribute the modified material.

4. **ShareAlike**: If you adapt the icons, you must distribute your contributions under the same license as the original.

5. **No Additional Restrictions**: You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.

## Disclaimer

The icons are provided "as-is", without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose, and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages, or other liability, whether in an action of contract, tort, or otherwise, arising from, out of, or in connection with the icons or the use or other dealings in the icons.

## Contact

For any questions or permissions beyond the scope of this license, you may contact:

**Ulisses Medien & Spiel Distribution GmbH**
Industriestraße 11
65529 Waldems / Steinfischbach

---

By using the icons in this repository, you agree to the terms outlined in this license.

