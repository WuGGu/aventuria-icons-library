# Aventuria Icons Library

This directory contains a collection of icons used on Aventuria game cards. These icons represent various instructions found on the cards and are intended to be used in our database where #Tags can be replaced by the corresponding icons.

## Icon List

Each icon corresponds to a specific instruction or element in the Aventuria game. Below is a list of the icons available in this library:

- **Abenteuermarke**: ![Abenteuermarke](./Abenteuermarke.png)
- **Action-Symbol**: ![Action-Symbol](./Action-Symbol.png)
- **Ausruestung**: ![Ausruestung](./Ausruestung.png)
- **Ausweichen-Symbol**: ![Ausweichen-Symbol](./Ausweichen-Symbol.png)
- **Begleiter-Symbol**: ![Begleiter-Symbol](./Begleiter-Symbol.png)
- **Fernkampf-1**: ![Fernkampf-1](./Fernkampf-1.png)
- **Fernkampf-2**: ![Fernkampf-2](./Fernkampf-2.png)
- **Fernkampf-3**: ![Fernkampf-3](./Fernkampf-3.png)
- **Fernkampf-Symbol**: ![Fernkampf-Symbol](./Fernkampf-Symbol.png)
- **Flucht-Symbol**: ![Flucht-Symbol](./Flucht-Symbol.png)
- **Hand**: ![Hand](./Hand.png)
- **Lebenspunkte-Symbol**: ![Lebenspunkte-Symbol](./Lebenspunkte-Symbol.png)
- **Liturgie-1**: ![Liturgie-1](./Liturgie-1.png)
- **Liturgie-2**: ![Liturgie-2](./Liturgie-2.png)
- **Magie-Symbol**: ![Magie-Symbol](./Magie-Symbol.png)
- **Nahkampf-1**: ![Nahkampf-1](./Nahkampf-1.png)
- **Nahkampf-2**: ![Nahkampf-2](./Nahkampf-2.png)
- **Nahkampf-3**: ![Nahkampf-3](./Nahkampf-3.png)
- **Nahkampf-Symbol**: ![Nahkampf-Symbol](./Nahkampf-Symbol.png)
- **NewTimeCounter-Symbol**: ![NewTimeCounter-Symbol](./NewTimeCounter-Symbol.png)
- **Nicht_bleibende_Karte_Symbol**: ![Nicht_bleibende_Karte_Symbol](./Nicht_bleibende_Karte_Symbol.png)
- **Rueckkehr-Symbol**: ![Rueckkehr-Symbol](./Rueckkehr-Symbol.png)
- **Ruestung-1**: ![Ruestung-1](./Ruestung-1.png)
- **Ruestung-2**: ![Ruestung-2](./Ruestung-2.png)
- **Ruestung-3**: ![Ruestung-3](./Ruestung-3.png)
- **Ruestungssymbol**: ![Ruestungssymbol](./Ruestungssymbol.png)
- **Schicksalspunkt**: ![Schicksalspunkt](./Schicksalspunkt.png)
- **Spieler-Symbol**: ![Spieler-Symbol](./Spieler-Symbol.png)
- **Talent**: ![Talent](./Talent.png)
- **Tap-Symbol**: ![Tap-Symbol](./Tap-Symbol.png)
- **Tier-Moral-Symbol**: ![Tier-Moral-Symbol](./Tier-Moral-Symbol.png)
- **Totenschaedel-Haertegrad**: ![Totenschaedel-Haertegrad](./Totenschaedel-Haertegrad.png)
- **Trefferpunkte-Symbol**: ![Trefferpunkte-Symbol](./Trefferpunkte-Symbol.png)
- **Umdreh-Symbol**: ![Umdreh-Symbol](./Umdreh-Symbol.png)
- **Verderbensmarke**: ![Verderbensmarke](./Verderbensmarke.png)
- **Vorteil**: ![Vorteil](./Vorteil.png)
- **Zauber-1**: ![Zauber-1](./Zauber-1.png)
- **Zauber-2**: ![Zauber-2](./Zauber-2.png)

## Usage

This repository provides an icon library for our database, enabling us to replace #Tags in text with the corresponding icons. For example, `#Talent` will be replaced by the `Talent.png` icon.

### Example

Given the text:

#Talent This represents a talent action.
The text can be processed to replace `#Talent` with the Talent icon:

![Talent](./Talent.png)

## Contributing

If you need to add more icons or update existing ones, please ensure the new icons are added to the `aventuria_icons` directory and update the `icons.json` file accordingly.

## License

The icons in this repository are the property of Ulisses Medien & Spiel Distribution GmbH.
